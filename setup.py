from setuptools import setup

setup(
    name='pyarcade',
    version='0.0.1',
    description='PyArcade',
    author= 'Usama Younus, Garrett Vanhoy',
    author_email='uyounus@umd.edu, gvanhoy@umd.edu',
    test_suite="tests"
)
