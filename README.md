# Assignment 10: Deploy to Cloud

This is the final assignment to deploy a Flask Rest Api in EC2 Terminal, and test the functionality of the database.

### How to Run on EC2/locally and Test
* **The Docker Way:**
To run _Pyarcade Rest Api_, we can build the image of the Rest Api and associated Database in Docker instance and then interactively run it using the following commands in order:
<pre>
docker-compose up
</pre>
Then, make sure run.sh is executable with:
<pre>
chmod u+x run.sh
</pre>
Finally, run the tests in the _run.sh_ file through below command:
<pre>
./run.sh localhost:80
</pre>

* **Local Terminal:**
The Api and Database need to be active and running on _EC2_ or a cloud_base to access through a local machine terminal.
To run _Pyarcade Rest Api_ run the below command from your computer terminal.
<pre>
`./run.sh ec2-3-22-99-28.us-east-2.compute.amazonaws.com:80`
</pre>

* **To Run the Tests Locally:**
Flask is configured in pyarcade_rest/__init__.py is configured to use an in-memory sqlite database if `FLASK_ENV=test` and to use a MySQL database if FLASK_ENV=production.

    You can configure FLASK_ENV in PyCharm by editing the run configurations (usually a dropdown in the top right with an option called edit configurations) and changing the Environment Variables to be `FLASK_ENV=test`

    After configuring _Test ENV_, follow below commands in sequence:
<pre>
pip install -r requirements.txt
python setup.py install
</pre>
